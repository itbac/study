1.AopTheoryApplication类是springboot的启动类，其中调用了服务，通过TimeConsumingStatisticsAspect类进行AOP切面进行耗时统计。

2.aopMain类是基于手写的AOP 与 IOC 实现的方法调用的耗时统计的Main方法入口。

3.zkTest类，是基于 curator 第三方客户端，测试手写分布式锁 zookeeper lock (有bug)。[zookeeper的安装方法](https://www.cnblogs.com/itbac/p/12240197.html)

4.ZkLock类，是基于I0Itec.zkclient.ZkClient第三方客户端的手写分布式锁。（感觉完美）

5.GuavaCacheDemo类，是guava本地缓存的简单使用。

6.QRCodeUtil类，提供二维码生成和解析工具类，支持二维码中有logo图片 