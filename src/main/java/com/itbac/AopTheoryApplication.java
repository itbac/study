package com.itbac;

import com.itbac.aop.theory.service.KtvService;
import com.itbac.aop.theory.service.SPAService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class AopTheoryApplication {
    public static void main(String[] args) {
        //启动
        ApplicationContext context = SpringApplication.run(AopTheoryApplication.class, args);
        //获取服务
        KtvService ktvService = context.getBean(KtvService.class);
        SPAService spa = context.getBean(SPAService.class);
        //调用服务的方法，可以
        // 使用AOP，计算方法调用的耗时
        ktvService.momoSing("bac");
        spa.aromaOilMassage("Mike");
        spa.fullBodyMassage("Tony");
        spa.aromaOilMassage("Allen");
    }
}
