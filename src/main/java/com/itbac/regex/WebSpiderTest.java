package com.itbac.regex;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.*;


/**
 * 网络爬虫取链接
 */
public class WebSpiderTest {
    public static void main(String[] args) {
        String content = getURLContent("http://www.163.com");

        List<String> list = getMatherSubstrs(content, "href=\"(http[\\w\\s./:]+?)\"");

        list.forEach(System.out::println);

    }

    /**
     *  匹配
     * @param destStr 目标字符串
     * @param regexStr 正则表达式
     * @return
     */
    public static List<String> getMatherSubstrs(String destStr,String regexStr){
        List<String> list = new ArrayList<>();
        //正则表达式
//        Pattern pattern = compile("href=\".+?\""); //取到超链接地址
//        Pattern pattern = compile("href=\"(.+?)\""); //取到超链接地址,（）分组 group(1)取括号里的值
        // "href=\"([\\w\\s./:]+?)\""   进一步过滤。
        //"href=\"(http[\\w\\s./:]+?)\""    http开头
        Pattern pattern = compile(regexStr);
        //匹配
        Matcher matcher = pattern.matcher(destStr);

        while (matcher.find()) {
            list.add(matcher.group(1));
        }
        return list;
    }

    //http://www.163.com
    //获取urlStr网页对应的源码内容
    public static String getURLContent(String urlStr) {
        StringBuilder sb = new StringBuilder();
        try {
            URL url = new URL(urlStr);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            url.openStream(),
                            //设置你所爬网页的编码格式
                            Charset.forName("gbk")));
            /**
             查看网页源码，看到编码格式
             <head>
             <meta http-equiv="Content-Type" content="text/html; charset=gbk">
             */
            String temp = null;
            while ((temp = bufferedReader.readLine()) != null) {
                sb.append(temp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
