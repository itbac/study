package com.itbac.util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.lang.reflect.Type;

public class GsonUtil { public static final Gson gson = new Gson();
    private static JsonParser jsonParser = null;

    public static String toJson(Object obj) {
        return gson.toJson(obj);
    }

    public static <T> T fromJson(String json, Class<T> c) {
        return gson.fromJson(json, c);
    }
    public static <T> T fromJson(String json,Type typeOfT) {
        return gson.fromJson(json, typeOfT);
    }
    public static <T> T fromJson(JsonElement json, Class<T> c) {
        return gson.fromJson(json, c);
    }
    public static <T> T fromJson(JsonElement json,Type typeOfT) {
        return gson.fromJson(json, typeOfT);
    }

    public static JsonParser getJsonParser(){
        if(jsonParser == null) {
            jsonParser = new JsonParser();
        }
        return jsonParser;
    }

    public static JsonElement parseAsJson(String json) {
        return getJsonParser().parse(json);
    }

}