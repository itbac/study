package com.itbac.util;


import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.UUID;

public class ExcelUtils {
    private static NumberFormat numberFormat=NumberFormat.getNumberInstance();

    public static Workbook openFile(String path){
        InputStream inp;
        try {
            inp = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            return null;
        }
        String [] paths = path.split("\\.");
        try {
            if ("xls".equals(paths[paths.length - 1])){
                return new HSSFWorkbook(inp);
            }else{
                return new XSSFWorkbook(inp);
            }
        } catch (IOException e) {
            return null;
        }
    }
    public static String getFileName(String url){
        String [] paths = url.split("\\.");
        if ("xls".equals(paths[paths.length - 1])){
            return String.format(UUID.randomUUID()+".xls");
        }else{
            return String.format(UUID.randomUUID()+".xlsx");
        }
    }
    public static int getCellIntValue(Cell cell){
        if(cell==null){
            return 0;
        }
        if(cell.getCellType()== CellType.NUMERIC){
            return (int)cell.getNumericCellValue();
        }else{
            return 0;
        }
    }
    public static double getCellDoubleValue(Cell cell){
        if(cell==null){
            return 0.0;
        }
        if(cell.getCellType()==CellType.NUMERIC){
            return cell.getNumericCellValue();
        }else{
            return 0.0;
        }
    }
    public static String getCellStringValue(Cell cell){
        if(cell==null){
            return "";
        }
        if(cell.getCellType()==CellType.STRING){
            return cell.getStringCellValue();
        }else if(cell.getCellType()==CellType.NUMERIC){
            return String.valueOf(numberFormat.format(cell.getNumericCellValue()).replaceAll(",",""));
        }else{
            return "";
        }
    }

}

