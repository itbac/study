package com.itbac.classLoader;

import java.io.*;
import java.util.Objects;

/**
 * 自定义类加载器
 * 文件类加载器
 */
public class FileSystemClassLoader extends ClassLoader {

    //路径  D:/myJava/
    //com.itbac.text.User  --> D:/myJava/com/itbac/text/User.class
    private String rootDir;

    public FileSystemClassLoader(String rootDir) {
        this.rootDir = rootDir;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        Class<?> c = findLoadedClass(name);
        if (Objects.nonNull(c)) {
            return c;
        }
        //获取父类加载器
        ClassLoader parent = this.getParent();
        // 父类加载
        try {
            c = parent.loadClass(name);
        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
        }
        if (Objects.nonNull(c)) {
            return c;
        }
        //类名转字节码
        byte[] classData = getClassData(name);
        if (Objects.isNull(classData)) {
            throw new ClassNotFoundException();
        }
       return defineClass(name, classData, 0, classData.length);
    }

    private byte[] getClassData(String name) {
      //  com.itbac.text.User  --> D:/myJava/com/itbac/text/User.class
        String path = rootDir + File.separator + name.replaceAll(".", File.separator) + ".class";
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        FileInputStream fileInputStream = null;
        try {
            //输入流
             fileInputStream = new FileInputStream(path);
            byte[] buffer = new byte[1024];
            int temp = 0;
            //读取
            while ((temp = fileInputStream.read(buffer))!=-1){
                //写出
                byteArrayOutputStream.write(buffer, 0, temp);
            }
            return byteArrayOutputStream.toByteArray();

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (Objects.nonNull(fileInputStream)) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                byteArrayOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return null;
    }
}
