package com.itbac.classLoader;

import java.io.File;

/**
 * 测试自定义类加载器
 */
public class ClassLoaderTest {
    public static void main(String[] args) throws ClassNotFoundException {

        FileSystemClassLoader classLoader = new FileSystemClassLoader("D:" + File.separator + "myjava");
        FileSystemClassLoader classLoader2 = new FileSystemClassLoader("D:" + File.separator + "myjava");
        //同一个类，被同一个类加载器加载，jvm认为是同一个类。
        Class<?> c1 = classLoader.loadClass("com.itbac.classLoader.HelloWorld");
        Class<?> c2 = classLoader.loadClass("com.itbac.classLoader.HelloWorld");
        //同一个类，被不同类加载器加载，jvm认为是不同的类。
        Class<?> c3 = classLoader2.loadClass("com.itbac.classLoader.HelloWorld");

        System.out.println(c1.hashCode());
        System.out.println(c2.hashCode());
        System.out.println(c3.hashCode());


        //设置线程上下文类加载器
        Thread.currentThread().setContextClassLoader(
                new FileSystemClassLoader("\"D:\" + File.separator + \"myjava\""));
        //用自定义类加载器，设置到线程上下文，在整个线程中都可以使用自定义加载器了。
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        Class<?> aClass = contextClassLoader.loadClass("com.itbac.bean.User");

    }
}
