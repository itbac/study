package com.itbac.translate;

import java.io.Serializable;
import java.util.List;

public class TransResult implements Serializable {

    //原语言
    public String from;
    //翻译后的语言
    public String to;
    //结果
    public List<TransResultData> trans_result;
}
