package com.itbac.translate;

import java.io.Serializable;

public class TransResultData implements Serializable{

    //原字符,src 是中文的 Unicode 编码
    public String src;
    //翻译结果
    public String dst;
}
