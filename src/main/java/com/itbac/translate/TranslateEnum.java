package com.itbac.translate;

public enum TranslateEnum {
    //自动检测
    auto("auto","自动检测"),
    //中文
    zh("zh","中文"),
    //英文
    en("en","英语"),



    ;
    private String type;
    private String name;

    TranslateEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }

    TranslateEnum() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
