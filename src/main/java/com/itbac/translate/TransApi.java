package com.itbac.translate;

import com.google.gson.JsonElement;
import com.itbac.util.GsonUtil;
import org.springframework.util.CollectionUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class TransApi {

    private static final String TRANS_API_HOST = "http://api.fanyi.baidu.com/api/trans/vip/translate";

    private String appid;
    private String securityKey;

    public TransApi(String appid, String securityKey) {
        this.appid = appid;
        this.securityKey = securityKey;
    }

    //获取英文
    public String getEnglish(String query) {
        String transResult = this.getTransResult(query, TranslateEnum.zh.getType(), TranslateEnum.en.getType());
        return this.parseJson(transResult);
    }
    //获取中文
    public String getZh(String query) {
        String transResult = this.getTransResult(query, TranslateEnum.en.getType(), TranslateEnum.zh.getType());
        return this.decode(transResult);
    }

    //解析json
    private String parseJson(String str) {
        JsonElement jsonElement = GsonUtil.parseAsJson(str);
        if (null != jsonElement) {
            TransResult transResult = GsonUtil.fromJson(jsonElement, TransResult.class);
            if (!CollectionUtils.isEmpty(transResult.trans_result)) {
                TransResultData transResultData = transResult.trans_result.get(0);
                return transResultData.dst;
            }
        }
        return null;
    }
    //中文，需要解码，从 Unicode 字符变成 utf-8
    private String decode(String str) {
        String unicodeString = this.parseJson(str);
        if (null == unicodeString) {
            return null;
        }
        try {
            return URLDecoder.decode(unicodeString, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }


    public String getTransResult(String query, String from, String to) {
        Map<String, String> params = buildParams(query, from, to);
        return HttpGet.get(TRANS_API_HOST, params);
    }

    private Map<String, String> buildParams(String query, String from, String to) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("q", query);
        params.put("from", from);
        params.put("to", to);

        params.put("appid", appid);

        // 随机数
        String salt = String.valueOf(System.currentTimeMillis());
        params.put("salt", salt);

        // 签名
        String src = appid + query + salt + securityKey; // 加密前的原文
        params.put("sign", MD5.md5(src));

        return params;
    }

}
