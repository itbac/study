package com.itbac.lock;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.framework.recipes.cache.NodeCache;
import org.apache.curator.framework.recipes.cache.NodeCacheListener;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.LockSupport;

/**
 * zk分布式锁
 * 临时顺序节点，解决惊群效应问题。
 * 使用 curator 第三方客户端
 */
public class ZookeeperLock {
    //根节点
    private static final String WORKSPACE = "/lock-workspace";
    //锁名称
    private String lockName;
    //zk客户端 ,也可以 zkClient
    private CuratorFramework client;
    //锁节点路径
    private String lockPath;
    //当前获得锁的路径
    private String currentLockPath;
    //路径，线程
    private Map<String, Thread> threadMap = new ConcurrentHashMap<String, Thread>();

    private ZkClient zkClient=new ZkClient("192.168.0.88:2181");

    public ZookeeperLock(String lockName) {
        this.lockName = lockName;
        lockPath = WORKSPACE + "/" + lockName;
        client = zkClient.getCuratorFramework();
    }

    //获取锁
    public void lock(){
        //创建临时顺序节点
        String tempPath = zkClient.getNewEphemeralSequentialNode(lockPath);
        System.err.println("创建临时顺序节点:" + tempPath);
        if (Long.parseLong(ZkClient.getNum(tempPath))!=0){
            //不是第一个节点
            String lastPath = ZkClient.getLastPath(tempPath);
            System.err.println("上一个节点，lastPath:" + lastPath);
            boolean existNode = zkClient.isExistNode(lastPath);
            if (existNode) {
                try {
                    //监听上一个节点并阻塞等待
                    this.listenAndWait(lastPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                System.out.println("! existNode:" + lastPath);
            }
        }else {
            //没有上一个节点
            System.out.println("第一个节点：" + tempPath);
        }
        //当前线程获得了锁，把节点保存起来。
        currentLockPath = tempPath;
        System.err.println("获得了锁："+currentLockPath+",Thread:"+Thread.currentThread().getName());
    }
    //释放锁
    public void unLock(){
        if (!StringUtils.isEmpty(currentLockPath)) {
            if (zkClient.isExistNode(currentLockPath)) {
                //删除当前获得锁的节点路径
                zkClient.deleteChildrenIfNeededNode(currentLockPath);
                System.err.println("释放了锁："+currentLockPath+",Thread:"+Thread.currentThread().getName());
            }
            Thread thread = threadMap.get(currentLockPath);
            if (null != thread) {
                LockSupport.unpark(thread);
                threadMap.remove(currentLockPath);
            }
        }
    }
    private void listenAndWait(final String path) {
        final Thread thread = Thread.currentThread();
        try {
            final NodeCache nodeCache = new NodeCache(client, path);
            //调用start方法开始监听
            nodeCache.start();
            //添加NodeCacheListener监听器
            nodeCache.getListenable().addListener(new NodeCacheListener() {
                public void nodeChanged() throws Exception {
                    //监听节点变化，有变化再查节点是否存在。
                    //唤醒当前线程
                    boolean existNode = zkClient.isExistNode(path);
                    if (!existNode) {
                        System.err.println("节点:"+path+"已被删除了1。唤醒线程:"+thread.getName());
                        LockSupport.unpark(thread);
                        threadMap.remove(path);
                        nodeCache.close();
                    }
                    ChildData currentData = nodeCache.getCurrentData();
                    if (null == currentData) {
                        System.err.println("节点:"+path+"已被删除了,currentData== null。唤醒线程:"+thread.getName());
                        LockSupport.unpark(thread);
                        threadMap.remove(path);
                        nodeCache.close();
                    }
                    System.out.println("监听节点变化："+nodeCache.getCurrentData());
                }
            });
            System.out.println("开始监听：" + path);
        } catch (Exception e) {
            e.printStackTrace();
            //唤醒当前线程
            LockSupport.unpark(thread);
            System.out.println("异常："+path);
        }
        boolean existNode = zkClient.isExistNode(path);
        if (!existNode) {
            System.err.println("节点:"+path+"已被删除了2。唤醒线程:"+thread.getName());
            LockSupport.unpark(thread);
        }
        //阻塞等待
        System.err.println("park:" + path);
        //保存线程
        threadMap.put(path, thread);
        LockSupport.park();
    }






}
