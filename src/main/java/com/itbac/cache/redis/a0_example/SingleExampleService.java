package com.itbac.cache.redis.a0_example;

import com.itbac.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * 简单示例
 */
@Service
public class SingleExampleService {
    //直接注入StringRedisTemplate ，则代表每一个操作参数都是字符串。
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //参数可以是任意对象，默认由JDK序列化
    @Autowired
    private RedisTemplate redisTemplate;

    //简单的缓存插入功能
    public void setUser(String userId, User user) {
        redisTemplate.opsForValue().set(userId, user);
    }

    public User findUser(String userId){
        User user = null;
        //1.判断缓存中是否存在
        user = (User) redisTemplate.opsForValue().get(userId);
        if (null != user) {
            System.out.println("从缓存中读取到的值：" + user.toString());
            return user;
        }
        //2.不存在，则读取数据库
        user = new User(18,userId, "张三");
        System.out.println("从数据库中读取到的值：" + user.toString());
        if (null != user) {
            redisTemplate.opsForValue().set(userId, user);
        }
        return user;
    }



}
