package com.itbac.cache.redis.a0_example;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 *  spring文档
 *  https://docs.spring.io/spring-data/redis/docs/2.1.5.RELEASE/reference/html/#redis
 */


@Configuration  //以注解的方式配置，不用xml .
@Profile("single") //需要指定环境生效，@ActiveProfile("single"),激活指定的环境
@EnableCaching
public class SingleRedisAppConfig {

    @Bean
    public LettuceConnectionFactory redisConnectionFactory(){
        System.out.println("使用单机版本");
        //Standalone 单机版
        //也可以用 JedisConnectionFactory 客户端
        return new LettuceConnectionFactory(new RedisStandaloneConfiguration("192.168.0.88", 6379));
    }
    @Bean
    public RedisTemplate redisTemplate(RedisConnectionFactory redisConnectionFactory){
        RedisTemplate redisTemplate = new RedisTemplate();
        //设置连接工厂
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        //可以配置对象的转换规则，比如使用json格式对 object 进行存储。
        //Object -> 序列化 -> 二进制流 ->  Redis-server 存储
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
        return redisTemplate;
    }

    //配置Spring Cache 注解功能
    @Bean
    public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {
        //不锁定 Redis 缓存编写器
        RedisCacheWriter redisCacheWriter = RedisCacheWriter.nonLockingRedisCacheWriter(redisConnectionFactory);
        //默认缓存配置
        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig();
        return new RedisCacheManager(redisCacheWriter, redisCacheConfiguration);
    }

}
