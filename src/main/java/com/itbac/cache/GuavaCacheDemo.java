package com.itbac.cache;

import com.google.common.cache.*;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

//guava本地缓存
public class GuavaCacheDemo {
    public static void main(String[] args) throws ExecutionException {
        //缓存借口这里是LoadingCache,LoadingCache在缓存项不存在时可以自动加载缓存
        LoadingCache<String, String> cache =
                //CacheBuilder的构造函数是私有的，只能通过其静态方法来获得实例
                CacheBuilder.newBuilder()
                        //设置并发级别为8，并发级别是指可以同时写缓存的线程数
                        .concurrencyLevel(8)
                        //设置写缓存后8秒钟过期
                        .expireAfterWrite(8, TimeUnit.SECONDS)
                        //设置写缓存后1秒钟刷新（不太明白）
                        //TODO  跟expire的区别是，指定时间过后，expire是remove该key，下次访问是同步去获取返回新值
                        //TODO  而refresh则是指定时间后，不会remove该key，下次访问会触发刷新，新值没有回来时返回旧值
                        .refreshAfterWrite(1, TimeUnit.SECONDS)
                        //设置缓存容量的初始容量为10
                        .initialCapacity(10)
                        //设置缓存最大容量为100，超过100之后就会按照LRU最近最少使用算法来移除缓存项
                        .maximumSize(100)
                        //设置要统计缓存的命中率
                        .recordStats()
                        //设置缓存的移除通知
                        .removalListener(new RemovalListener<Object, Object>() {
                            @Override
                            public void onRemoval(RemovalNotification<Object, Object> removalNotification) {
                                System.out.println("通知："+removalNotification.getKey() + "被移除了，原因：" + removalNotification.getCause());
                            }
                        })
                        //build 方法中可以指定CacheLoader,在缓存不存在时通过CacheLoader的实现自动加载缓存
                        .build(new CacheLoader<String, String>() {
                            @Override
                            public String load(String key) throws Exception {
                                System.out.println("缓存没有时，从数据库加载:"+key);
                                //TODO 查询数据库的代码忽略
                                return "bac:" + key;
                            }
                        });

        //第一次读取
        for (int i = 0; i < 20; i++) {
            String value = cache.get("id_" + i);
            System.out.println(value);
        }
        System.out.println("————————分割线————————");
        //第二次读取
        for (int i = 0; i < 20; i++) {
            String value = cache.get("id_" + i);
            System.out.println(value);
        }
        System.out.println("————————分割线————————");
        System.out.println("cache stats:");
        //最后打印缓存的命中率等情况
        System.out.println(cache.stats().toString());



    }
}
