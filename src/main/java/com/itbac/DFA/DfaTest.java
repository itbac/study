package com.itbac.DFA;

import cn.hutool.dfa.WordTree;

import java.util.*;

/**
 * DFA算法实现敏感词过滤
 */
public class DfaTest {
    public static void main(String[] args) {
        String text = "太多的伤感情怀也许只局限于饲养基地 荧幕中的情节，主人公尝试着去用某种方式" +
                "渐渐的很潇洒地释自杀指南怀那些自己经历的伤感。" +
                "然后法轮功我们的扮演的角色就是跟随着主人公的喜红客联盟 怒哀乐而过于牵强的" +
                "把自己的情感也附加于银幕情节中，然后感动就流泪," +
                "难过就躺在某一个人的怀里尽情的阐述心扉fuck或者手机卡复制器一个人一杯红酒" +
                "一部电影在夜三级片深人静的晚上，关上电话静静的发呆着。这辈子是不可能打工的";
        //设置敏感词库
        WordTree tree = new WordTree();
        tree.addWord("法轮功");
        tree.addWord("三级片");
        tree.addWord("自杀");
        tree.addWord("红客联盟");
        tree.addWord("出锅");
        tree.addWord("土豆");
        tree.addWord("fuck");

        //isDensityMatch 是密度匹配。false 跳过已经匹配的关键词 ，true 不跳过已经匹配的关键词
        //isGreedMatch 是贪心匹配。 false 匹配到最短关键词 ，true 匹配到最长关键词
        List<String> list = tree.matchAll(text, -1, true, true);
        System.out.println(list);


    }

}
