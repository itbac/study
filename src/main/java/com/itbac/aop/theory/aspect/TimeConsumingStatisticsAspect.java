package com.itbac.aop.theory.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

//耗时统计切面类
//@Component
//@Aspect
public class TimeConsumingStatisticsAspect {
    //包路径下，所有类，所有方法，任意参数
    @Around("execution(* com.itbac.aop.theory.service.*.*(..))")
    public Object methodTimeConsumingStatistics(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();
        long end = System.currentTimeMillis();
        System.out.println("记录：" + joinPoint.getTarget().toString() + "," +
                joinPoint.getSignature().toString() + "耗时：" + (end - start)+"毫秒");
        return proceed;
    }
    
    
}
