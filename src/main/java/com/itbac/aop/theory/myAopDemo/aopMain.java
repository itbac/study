package com.itbac.aop.theory.myAopDemo;

import com.itbac.aop.theory.ioc.MyApplicationContext;
import com.itbac.aop.theory.ioc.MyBeanDefinition;
import com.itbac.aop.theory.ioc.bacAplicationContext;
import com.itbac.aop.theory.myAop.Advice;
import com.itbac.aop.theory.myAop.MyAspect;
import com.itbac.aop.theory.myAop.Pointcut;
import com.itbac.aop.theory.myAop.TimeCsAdvice;
import com.itbac.aop.theory.service.KtvPrincessB;
import com.itbac.aop.theory.service.KtvService;
import com.itbac.aop.theory.service.SPAService;
import com.itbac.aop.theory.service.SpaPrincessA;

//用户使用手写的AOP
public class aopMain {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        //增强通知
        Advice advice = new TimeCsAdvice();
        //切入点
        Pointcut pointcut1 = new Pointcut(
                "com\\.itbac\\.aop\\.theory\\.service\\..*",
                ".*Massage");
        Pointcut pointcut2 = new Pointcut(
                "com\\.itbac\\.aop\\.theory\\.service\\..*",
                ".*momoSing");
        //我的切面类
        MyAspect aspect1 = new MyAspect(pointcut1, advice);
        MyAspect aspect2 = new MyAspect(pointcut2, advice);
        //要织入， 让切面生效的过程，叫织入。
        //能让用户自己去new 对象吗？不能。应该要有一个工厂来负责

        //一定不能让用户拿到原始的对象，而是被我动了手脚的对象。
        //提供一个工厂来为用户提供业务对象。 IOC 是 AOP 的基石
        //需要IOC

        //创建我的IOC容器
        MyApplicationContext context = new bacAplicationContext();
        //在Bean定义中设置切面,织入
        //用户注册Bean定义信息  >>  在spring 中 是使用XML 或注解
        context.registerBeanDefinition("spa", new MyBeanDefinition(SpaPrincessA.class,aspect1));
        context.registerBeanDefinition("ktv", new MyBeanDefinition(KtvPrincessB.class,aspect2));
        //从IOC 容器中 getBean
        SPAService spa = (SPAService)context.getBean("spa");
        //调用方法（表达式切中了）
        spa.aromaOilMassage("bac");
        KtvService ktv = (KtvService)context.getBean("ktv");
        //调用方法（表达式没切中）
        ktv.momoSing("mike");

    }
}
