package com.itbac.aop.theory.myAop;

import java.lang.reflect.Method;
//增强通知
public interface Advice {
    //定义一个怎么样的方法？
    //由用户提供实现它提供增强逻辑
    //我可以在方法执行前， 获得当前时间
    //当前是什么方法，参数是什么
    //类，方法，参数
    Object invoke(Object target, Method method, Object[] args) throws Exception;
}
