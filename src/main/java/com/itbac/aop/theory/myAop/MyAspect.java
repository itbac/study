package com.itbac.aop.theory.myAop;
//我的切面 Aspect = 切入点 PointCut  + 增强通知 advice
public class MyAspect {
    //切入点
    private Pointcut pointcut;
    //增强通知
    private Advice advice;

    public MyAspect() {
    }

    public MyAspect(Pointcut pointcut, Advice advice) {
        this.pointcut = pointcut;
        this.advice = advice;
    }

    public Pointcut getPointcut() {
        return pointcut;
    }

    public void setPointcut(Pointcut pointcut) {
        this.pointcut = pointcut;
    }

    public Advice getAdvice() {
        return advice;
    }

    public void setAdvice(Advice advice) {
        this.advice = advice;
    }
}
