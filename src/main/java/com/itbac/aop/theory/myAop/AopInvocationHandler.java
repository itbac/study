package com.itbac.aop.theory.myAop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class AopInvocationHandler implements InvocationHandler {
    //目标对象
    private Object target;

    //切面
    private MyAspect aspect;

    public AopInvocationHandler() {
    }

    public AopInvocationHandler(Object target, MyAspect aspect) {
        this.target = target;
        this.aspect = aspect;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //应用增强逻辑 Advice
        //判断方法要不要被增强
        if (method.getName().matches(this.aspect.getPointcut().getMethodPattern())) {
            return this.aspect.getAdvice().invoke(target, method, args);
        }
        //不需要代理增强
        return method.invoke(target, args);
    }
}
