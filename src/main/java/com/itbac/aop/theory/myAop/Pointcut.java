package com.itbac.aop.theory.myAop;
//切入点
public class Pointcut {
    //类名匹配模式（这里我们用正则表达式）
    private String classPattern;
    //方法名匹配模式（这里我们用正则表达式）
    private String methodPattern;

    public Pointcut() {
    }

    public Pointcut(String classPattern, String methodPattern) {
        this.classPattern = classPattern;
        this.methodPattern = methodPattern;
    }

    public String getClassPattern() {
        return classPattern;
    }

    public void setClassPattern(String classPattern) {
        this.classPattern = classPattern;
    }

    public String getMethodPattern() {
        return methodPattern;
    }

    public void setMethodPattern(String methodPattern) {
        this.methodPattern = methodPattern;
    }
}
