package com.itbac.aop.theory.myAop;

import java.lang.reflect.Method;
//增强通知
public class TimeCsAdvice implements Advice {

    public Object invoke(Object target, Method method, Object[] args) throws Exception {
        long start = System.currentTimeMillis();
        Object ret = method.invoke(target, args);
        long end = System.currentTimeMillis();
        System.out.println("手写AOP,记录：" + target.getClass().getName() + "," +
                method.getName() + "耗时：" + (end - start)+"毫秒");
        return ret;
    }
}
