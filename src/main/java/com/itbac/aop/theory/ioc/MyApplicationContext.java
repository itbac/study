package com.itbac.aop.theory.ioc;

import com.itbac.aop.theory.myAop.MyAspect;

//IOC容器
public interface MyApplicationContext {

    Object getBean(String beanName) throws InstantiationException, IllegalAccessException;
    //注册bean定义信息
    void registerBeanDefinition(String beanName, MyBeanDefinition beanDefinition);



}
