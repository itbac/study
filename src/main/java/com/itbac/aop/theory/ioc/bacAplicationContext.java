package com.itbac.aop.theory.ioc;

import com.itbac.aop.theory.myAop.AopInvocationHandler;
import com.itbac.aop.theory.myAop.MyAspect;

import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class bacAplicationContext implements MyApplicationContext {
    //bean 定义信息
    private Map<String, MyBeanDefinition> beanDefinitionMap = new ConcurrentHashMap<String, MyBeanDefinition>();
    //单例池
    private Map<String, Object> beanMap = new ConcurrentHashMap<String, Object>();

    public Object getBean(String beanName) throws InstantiationException, IllegalAccessException {
        //我怎么知道这个名字对应的是什么bean ?    名字 >>  对应 的是 哪个类  ，bean定义信息。从哪来？
        //bean是用户定义，bean定义信息由用户给出，给到我
        //创建对象
        Object bean = null;
        bean = beanMap.get(beanName);
        if (null == bean) {
            //创建bean实例
            bean = createBeanInstance(beanName);
        }

        beanMap.put(beanName, bean);
        return bean;
    }
    //代理增强  ,JDK动态代理 ,或 cglib 代理
    private Object proxyEnhance(Object bean,MyAspect aspect) {
        return Proxy.newProxyInstance(bean.getClass().getClassLoader(), bean.getClass().getInterfaces(),
                new AopInvocationHandler(bean, aspect));
    }
    //创建bean实例
    private Object createBeanInstance(String beanName) throws IllegalAccessException, InstantiationException {
        MyBeanDefinition myBeanDefinition = this.beanDefinitionMap.get(beanName);
        Object bean = myBeanDefinition.getaClass().newInstance();
        MyAspect aspect = myBeanDefinition.getAspect();
        //织入。 要看要不要进行AOP增强，如果要，则不能返回原始对象。 AOP的原理，代理对象
        if (aspect != null && bean.getClass().getName().matches(aspect.getPointcut().getClassPattern())) {
            //切面非空，类名匹配，增强
            bean = proxyEnhance(bean,aspect);
        }
        return bean;
    }

    public void registerBeanDefinition(String beanName, MyBeanDefinition beanDefinition) {
        //保存好这个信息
        this.beanDefinitionMap.put(beanName, beanDefinition);
    }

}
