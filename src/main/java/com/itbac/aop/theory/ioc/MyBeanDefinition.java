package com.itbac.aop.theory.ioc;

import com.itbac.aop.theory.myAop.MyAspect;
//bean 定义信息
public class MyBeanDefinition {
    //类
    private Class aClass;
    //用一个切面，代表多个切面
    private MyAspect aspect;

    public MyBeanDefinition() {
    }

    public MyBeanDefinition(Class aClass) {
        this.aClass = aClass;
    }

    public MyBeanDefinition(Class aClass, MyAspect aspect) {
        this.aClass = aClass;
        this.aspect = aspect;
    }

    public Class getaClass() {
        return aClass;
    }

    public void setaClass(Class aClass) {
        this.aClass = aClass;
    }

    public MyAspect getAspect() {
        return aspect;
    }

    public void setAspect(MyAspect aspect) {
        this.aspect = aspect;
    }
}
