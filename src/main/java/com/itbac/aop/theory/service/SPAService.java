package com.itbac.aop.theory.service;
//SPA管理
public interface SPAService {
    //全身按摩
    void fullBodyMassage(String customer);
    //芳香精油按摩
    void aromaOilMassage(String customer);
    //休息
    void rest();

}
