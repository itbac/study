package com.itbac.aop.theory.service;

import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.TimeUnit;
@Service
public class KtvPrincessB implements KtvService {
    private Random random = new Random();

    private int bound = 5;

    public void momoSing(String customer) {
        try {
            TimeUnit.SECONDS.sleep(random.nextInt(bound));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(customer+"享受完了momoSing服务");
    }
}
