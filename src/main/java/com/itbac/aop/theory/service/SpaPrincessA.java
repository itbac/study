package com.itbac.aop.theory.service;

import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.TimeUnit;
@Service
public class SpaPrincessA implements SPAService {
    private Random random = new Random();

    private int bound = 5;

    public void fullBodyMassage(String customer) {
        try {
            TimeUnit.SECONDS.sleep(random.nextInt(bound));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(customer+"享受完了fullBodyMassage服务");
    }

    public void aromaOilMassage(String customer) {
        try {
            TimeUnit.SECONDS.sleep(random.nextInt(bound));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(customer+"享受完了aromaOilMassage服务");
    }

    public void rest() {
        try {
            TimeUnit.SECONDS.sleep(random.nextInt(bound));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(this + "休息完！");
    }
}
