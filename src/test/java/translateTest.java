import com.itbac.translate.TransApi;

/**
 * 百度翻译测试
 */
public class translateTest {

    private static final String APP_ID = "账户";
    private static final String SECURITY_KEY = "密码";


    public static void main(String[] args) {

        TransApi api = new TransApi(APP_ID, SECURITY_KEY);

        String english = api.getEnglish("人生最有价值的时刻,不是最后的功成名就,而是对未来充满期待与不安之时");
        System.out.println(english);

        //The most valuable moment in life is not the final success,
        // but the time full of expectation and anxiety for the future
    }
}
