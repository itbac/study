import com.itbac.util.QRCodeUtil;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class redisTest {
    @Test
    public void test1(){
        //使用 redis缓存
        JedisPool jedisPool = new JedisPool(new GenericObjectPoolConfig(), "192.168.0.88", 6379, 1000, "root");
        Jedis jedis = jedisPool.getResource();
        String set = jedis.set("name", "张三");
        String name = jedis.get("name");
        System.out.println(name);
        System.out.println(set);
    }

    @Test
    public void test2() throws Exception {
        try {
            //生成带有logo的二维码
            QRCodeUtil.encode("http://www.baidu.com", "D:\\java\\图片\\美女.jpg", "D:\\java\\图片", "我的二维码", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
