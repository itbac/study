import com.google.common.base.Charsets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * 阿里云大学教程
 * https://edu.aliyun.com/lesson_1012_8979?spm=5176.10731542.0.0.32546d96zUmZKe#_8979
 */

public class OutputStreamText {
    public static void main(String[] args) throws IOException {
        //File.separator  分隔符
        File file = new File("D:" + File.separator + "javaIo" + File.separator + "bac.txt");
        if (!file.getParentFile().exists()) {
            //父路径不存在，则创建父路径
            file.getParentFile().mkdirs();
        }
        //创建输出流，append = true ,追加到文件中
        OutputStream outputStream = new FileOutputStream(file,true);
        //输出内容，带换行
        String str = "www.itbac.com\r\n";
        //输出
        outputStream.write(str.getBytes(Charsets.UTF_8));
        //关闭流
        outputStream.close();


    }
}
