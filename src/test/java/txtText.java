import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

class MathUtil{
    private MathUtil(){}
    //四舍五入 ，保留几位小数
    public static double round(double num,int scale){
        return Math.round(Math.pow(10, scale) * num) / Math.pow(10, scale);
    }
}


public class txtText {
    public static void main(String[] args) throws IOException {

        //File.separator 路径分隔符
        File file = new File("D:"
                + File.separator + "javaIo"
                + File.separator + "myJson.txt");
        //如果是多级路径，就要创建父路径
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        //文件不存在
        if (!file.exists()) {
            file.createNewFile();
            System.out.println("创建文件");
        }
//        else {
//            file.delete();
//            System.out.println("删除文件");
//        }
        System.out.println("文件可读："+file.canRead());
        System.out.println("文件可写："+file.canWrite());
        System.out.println("文件可执行："+file.canExecute());
        //获取文件长度： 字节 bit，变成 K ,再变成 M ，四舍五入
        System.out.println("文件大小："+MathUtil.round(file.length()/(double)1024/1024,2));
        System.out.println("文件的最后修改时间："+
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").
                        format(new Date(file.lastModified())));
        System.out.println("是目录吗？" + file.isDirectory());
        System.out.println("是文件吗？" + file.isFile());
        if (file.isDirectory()) {
            //获取目录下的文件 ,可以写递归调用
            File[] files = file.listFiles();
            for (File file1 : files) {
                System.out.println(file1);
            }
        }
        File newFile = new File("d："+File.separator+"javaIo"+"aaa.txt");
        //重命名
        file.renameTo(newFile);


    }
}
