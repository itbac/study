package com.itbac.cache.redis.a0_example;

import com.itbac.AopTheoryApplication;
import com.itbac.bean.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AopTheoryApplication.class)
@ActiveProfiles("single")
public class SingleTests {
    @Autowired
    SingleExampleService singleExampleService;

    @Test
    public void setTest() {
        singleExampleService.setUser("tony", new User(18, "001", "tony"));
    }

    @Test
    public void getTest() {
        User tony = singleExampleService.findUser("001");
        System.out.println(tony.toString());
    }


}
