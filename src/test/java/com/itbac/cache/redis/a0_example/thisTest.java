package com.itbac.cache.redis.a0_example;

import static com.itbac.cache.redis.a0_example.User.printCompany;

class User{
    int id ;
    String name;
    String pwd;


    public User() {
        System.out.println("无参构造，在堆中创建对象。");
    }
    public static String company = "阿里巴巴";
    static {
        company ="腾讯";
        System.out.println("class字节码加载到内存的方法区,静态变量,静态代码块，静态方法,都在方法区");
    }


    public static void printCompany(){
        System.out.println("静态方法："+company);
    }
}

public class thisTest {

    public static void main(String[] args) {

        System.out.println("程序启动,main方法,是程序的入口");

        User user = new User();
        printCompany();








    }


}
