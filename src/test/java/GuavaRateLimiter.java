import com.google.common.util.concurrent.RateLimiter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 高性能限流器 Guava RateLimiter
 */
public class GuavaRateLimiter {
    public static void main(String[] args) {
        //限流器流速：2个请求 / 秒
        RateLimiter limiter = RateLimiter.create(2);
        //创建固定线程数的线程池
        ExecutorService pool = Executors.newFixedThreadPool(1);
        //记录上一次执行时间 (并发安全容器)
        AtomicLong begin = new AtomicLong(System.currentTimeMillis());
        for (int i = 0; i < 1000; i++) {
            //使用限流器
            limiter.acquire();
            int finalI = i;
            pool.execute(()->{
                long end = System.currentTimeMillis();
                System.out.println("i="+finalI +",时间差：" + (end - begin.get()));
                begin.set(end);
            });
        }
    }
}
