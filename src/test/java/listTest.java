import java.util.ArrayList;
import java.util.List;

public class listTest {
    public static void main(String[] args) {

        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        List<Integer> list2 = new ArrayList<>();
        list2.add(1);
        list2.add(2);
        /**
         * 解释：
         * 1. list1 会保留交集，删除其他元素
         * 2. list1变化了，返回值就是true ，不变，返回值就是false
         */
        boolean b = list1.retainAll(list2);
        System.out.println("b:" + b);
        System.out.println("list1:" + list1.toString());
        System.out.println("list2:" + list2.toString());


    }
}
