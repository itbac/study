import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 获取省市区
 */
public class JsoupTest {


    public static void main(String[] args) throws InterruptedException {

        //起始地址
        String url = "http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2018/";

        int curDepth = 1;
        //爬取深度
        int maxDepth = 3;

        Document seedDocument = connect(url, 5, 0);
        Area root = new Area(null,"", "");
        try {
            parseDocument(root, seedDocument, maxDepth, curDepth, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("按行输出：---------------------");
        print(root, "");
    }

    static void print(Area area, String allParentName) {
        String split = ",";
        String line = allParentName + split + area.id + split + area.name;
        if (area.areas == null || area.areas.isEmpty()) {
            String replace = line.replace(",,,", "");

            System.out.println("输出：" + replace);

            return;
        }
        for (Area child : area.areas) {
            print(child, line);
        }
    }

    static void parseDocument(Area parentArea, Document document, int maxDepth, int curDepth, int sleep) throws Exception {
        if (document == null) {
            return;
        }
        String cssQuery = ".provincetr td,.citytr,.countytr";
        Elements elements = document.select(cssQuery);
        for (Element element : elements) {
            Elements areaEle = element.select("a");
            if (areaEle.size() < 1) {
                areaEle = element.select("td");
                if (areaEle.size() > 1) {
                    Area area = getArea(parentArea, curDepth, areaEle);
                    parentArea.areas.add(area);
                    continue;
                }
                System.out.println("找不到相关a标签，跳过：" + element);
                continue;
            }
            //第一层为省份
            Area area = getArea(parentArea, curDepth, areaEle);
            parentArea.areas.add(area);

            if (maxDepth > curDepth) {
                String href = areaEle.get(0).absUrl("href");
                Thread.sleep(sleep);
                Document childDocument = connect(href, 5, 0);
                parseDocument(area, childDocument, maxDepth, curDepth + 1, sleep);
            }
        }
    }

    private static Area getArea(Area parentArea, int curDepth, Elements areaEle) {
        if (curDepth == 1) {
            //省份
            String id = areaEle.get(0).attr("href").replace(".html", "");
            return new Area(parentArea, id, areaEle.get(0).text());
        }
        String name = areaEle.get(1).text();
        name = !"市辖区".equals(name) ? name : parentArea.name;
        return new Area(parentArea, areaEle.get(0).text(), name);
    }

    static Document connect(String url, int maxTry, int curTimes) {
        try {
            System.out.println("请求：" + url);
            return Jsoup.connect(url).timeout(5000).get();
        } catch (IOException e) {
            e.printStackTrace();
            if (maxTry > curTimes) {
                System.out.println("请求：" + url + " 失败，重试第" + (curTimes + 1) + "次");
                return connect(url, maxTry, curTimes + 1);
            }
            return null;
        }
    }

    public static class Area {
        public Area parent;
        public String id;
        public String name;
        public List<Area> areas = new ArrayList<>();

        public Area(Area parent, String id, String name) {
            this.parent = parent;
            this.id = id;
            this.name = name;
        }

    }




}
