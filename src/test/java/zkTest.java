import com.itbac.lock.ZookeeperLock;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class zkTest {
    //随机数
    private static Random random = new Random();

    private static volatile int a = 10000;

    public static void main(String[] args) throws Exception {

        final ZookeeperLock lock = new ZookeeperLock("aaa");
        ExecutorService executorService = Executors.newFixedThreadPool(200);
        for (int i = 0; i < 200; i++) {
            executorService.submit(new Runnable() {
                public void run() {
                    //加锁。
                    lock.lock();
                    try {
                        a = --a;
                        //睡眠
                        TimeUnit.SECONDS.sleep(random.nextInt(5));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.err.println("输出：" + a);
                    //释放锁
                    lock.unLock();
                }
            });
        }
    }


}
